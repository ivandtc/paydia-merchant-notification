module.exports.environment = process.env.NODE_ENV || 'development';
module.exports.port = process.env.PORT || 3000;