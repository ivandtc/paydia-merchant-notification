const mysql = require('mysql2');

let pool = mysql.createPool({
  host: '192.168.100.8' || process.env.NODE_DB_HOST,
  database: 'paydia_dev_db' || process.env.NODE_ENV_BACKEND,
  user: 'dev' || process.env.NODE_DB_USER,
  password: 'dev123' || process.env.NODE_DB_PASSWORD
});

module.exports = pool.promise();