const router = require('express').Router();
const notificationController = require('../controllers/notification');

router.post('/', notificationController.getAllNotification);
router.post('/create', notificationController.insertNotification);
router.post('/update', notificationController.updateNotification);

module.exports = router;