const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const compression = require('compression');
const morgan = require('morgan');
const helmet = require('helmet');

const dbBackend = require('./util/database-backend');
const port = require('./util/env-data').port;
const environment = require('./util/env-data').environment;

const notification = require('./routes/notification');

process.on('uncaughtException', (error) => console.log('UNCAUGHT EXCEPTION', error));
process.on('unhandledRejection', (error) => console.log('UNHANDLED REJECTION', error));

if (environment === 'development') app.use(morgan('combined'));
app.use(helmet());
app.use(compression());
app.use(cors({
  credentials: true,
  origin: true
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use('/notification', notification);

app.get('/healthcheck', (req, res) => {
  res.send(true);
});

app.use((req, res) => { // handle 404
  res.send({
    status: false,
    data: [],
    error: [],
    pageNotFound: true
  });
});

if (!module.parent) {
  dbBackend.getConnection().then(conn2 => {
    console.log('Connected to Db Backend');
    app.listen(port, () => {
      console.log("Server Paydia Merchant Notification started on port " + port);
    });
  }).catch(err => {
    console.log('Failed Connect to Db Backend');
  })
}

module.exports = app;

