const dbBackend = require('../util/database-backend');
const processDate = require('../global-function/process-date');

module.exports = class Notification {
  static insertNotification (dataNotif, merchantId) {
    return dbBackend.execute(`INSERT INTO merchant_notif (merchant_id, notif_tgl, notif_data, notif_status) VALUES (?, ?, ?, ?)`, [merchantId, processDate(), JSON.stringify(dataNotif.notif_data), "0"]);
  }

  static getNotificationByMerchantId (merchantId) {
    return dbBackend.execute(`SELECT notif_id, notif_tgl, notif_data, notif_status FROM merchant_notif WHERE merchant_id = ?`, [merchantId]);
  }

  static updateNotification (data) {
    return dbBackend.execute(`UPDATE merchant_notif SET notif_status = ? WHERE merchant_id = ? AND notif_id = ?`, ['1', data.merchant_id, data.notif_id]);
  }
}