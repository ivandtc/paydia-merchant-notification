const returnData = require('../global-function/return-data');
const sendResponse = require('../global-function/send-response');
const handleError = require('../global-function/handle-error');
const Notification = require('../models/notification');
const errorMessage = require('../global-function/error-message');

exports.insertNotification = async (req, res, next) => {
  let { status, data, error } = returnData();
  try {
    let objNotif = {
      ...req.body.data
    }
    let [resultInsert] = await Notification.insertNotification(objNotif, req.body.where.merchant_id);
    if (resultInsert.affectedRows < 1) throw({type: errorMessage.config});
    status = true;
  } catch (err) {
    error = handleError(err);
  } finally {
    sendResponse(req, res, status, data, error);
  }
}

exports.getAllNotification = async (req, res, next) => {
  let { status, data, error } = returnData();
  try {
    if (!req.body.where) throw({type: errorMessage.config});
    if (!req.body.where.merchant_id) throw({type: errorMessage.config});
    let [notificationList] = await Notification.getNotificationByMerchantId(req.body.where.merchant_id);
    if (notificationList.affectedRows < 1) throw({type: errorMessage.config});
    let objNotif = [];
    if (notificationList.length > 0) {
      objNotif = notificationList.map((value) => {
        if (value.notif_data) value.notif_data = JSON.parse(value.notif_data);    
        return value;
      })      
    }
    data = objNotif;
    status = true;
  } catch (err) {
    error = handleError(err);
  } finally {
    sendResponse(req, res, status, data, error);
  }
}

exports.updateNotification = async (req, res, next) => {
  let { status, data, error } = returnData();

  try {
    if (!req.body.where) throw({type: errorMessage.config});
    if (!req.body.where.notif_id) throw({type: errorMessage.config});
    if (!req.body.where.merchant_id) throw({type: errorMessage.config});
    let [resultUpdate] = await Notification.updateNotification(req.body.where);
    console.log(resultUpdate)
    status = true;
  } catch (err) {
    error = handleError(err);
  } finally {
    sendResponse(req, res, status, data, error);
  }
}