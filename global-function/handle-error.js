module.exports = (error) => {
  try {
    let fixError = { type: 'error.config' };
    let internalErrors = ['SyntaxError', 'EvalError', 'RangeError', 'ReferenceError', 'TypeError', 'URIError', 'Error'];
    if (!internalErrors.includes(error.name)) {
      fixError = error;
    }
    return fixError;  
  } catch (error) {
    return fixError;
  }
}