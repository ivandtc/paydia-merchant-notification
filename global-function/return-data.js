module.exports = () => {
  return new Object({ 
    status: false, 
    data: [], 
    error: { 
      type: 'error.general' 
    } 
  });
}