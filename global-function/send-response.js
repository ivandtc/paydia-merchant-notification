function sendResponse (req, res, status = false, data = [], error = { type: 'error.general' }) {
  let objData = {
    status: status,
    data: data,
    error: error
  }
  if (objData.status) delete objData.error;
  res.send(objData);
}

module.exports = sendResponse;