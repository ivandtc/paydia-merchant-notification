module.exports = {
  general: 'error.general',
  config: 'error.config',
  message: 'error.message',
  validation: 'error.validation',
  required: 'error.required'  
}